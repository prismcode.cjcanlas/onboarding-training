const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Index.vue') }]
  },
  {
    path: '/notes',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Notes.vue') }]
  },
  {
    path: '/registration',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Registration.vue') },
      {
        name: 'complete',
        path: 'complete',
        component: () => import('src/pages/RegistrationComplete.vue')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
